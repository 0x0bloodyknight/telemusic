from ..models import User, Chat, Song


class DatabaseMethods:
    async def play(self, chat_id, song):
        queue, _ = await Chat.get_or_create(id=chat_id)
        await queue.songs.add(song)

    async def add_skip_vote(self, chat_id, user_id):
        user, _ = await User.get_or_create(id=user_id)
        queue, _ = await Chat.get_or_create(id=chat_id)
        await queue.skipping.add(user)
    
    async def clear_queue(self, chat_id):
        queue, _ = await Chat.get_or_create(id=chat_id)
        await queue.songs.clear()

    async def clear_skip_vote(self, chat_id):
        queue, _ = await Chat.get_or_create(id=chat_id)
        await queue.skipping.clear()

    async def is_queue_empty(self, chat_id):
        return not await Song.filter(queue__id=chat_id).exists()

    async def next_song(self, chat_id):
        song = await Song.filter(queue__id=chat_id).first()
        if song:
            await self.clear_skip_vote(chat_id)
            await song.delete()

    async def get_current_song(self, chat_id):
        song = await Song.filter(queue__id=chat_id).first()
        # second param is place
        return song, 0

    async def get_song(self, chat_id):
        return await Song.filter(queue__id=chat_id).first()

    async def get_queue(self, chat_id, offset=0, limit=20):
        return await Song.filter(queue__id=chat_id).offset(offset).limit(limit)

    async def is_skipping(self, chat_id, user_id):
        return await User.filter(skipping__id=chat_id, id=user_id).exists()
    
    async def count_skippers(self, chat_id):
        return await User.filter(skipping__id=chat_id).count()
