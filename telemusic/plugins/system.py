import time
import asyncio
import logging

from pyrogram.filters import command
from pyrogram.types import Message

from ..telemusic import Telemusic
from ..helpers import admins_only
from ..constants import (EVAL_RUNNING_TEXT, EVAL_ERROR_TEXT, EVAL_SUCCESS_TEXT,
                         EVAL_RESULT_TEXT, EVAL_TIMEOUT_TEXT, EVAL_TIMEOUT)
                    

log = logging.getLogger(__name__)


@Telemusic.on_message(command("ping"))
async def ping(_, message: Message):
    start = time.time()
    reply = await message.reply_text("...")
    delta_ping = time.time() - start
    await reply.edit_text(f"**Pong!** `{delta_ping:.3f} ms`")


@Telemusic.on_message(command("e"))
@admins_only
async def eval_expression(client: Telemusic, message: Message):
    expression = " ".join(message.command[1:])
    m = await message.reply_text(EVAL_RUNNING_TEXT.format(expression))

    try:
        result = await asyncio.wait_for(
                client.loop.run_in_executor(None, eval, expression),
                timeout=EVAL_TIMEOUT
                )
    except Exception as error:
        await client.edit_message_text(
            m.chat.id,
            m.message_id,
            EVAL_ERROR_TEXT.format(expression, error)
        )
    except asyncio.TimeoutError:
        await message.reply(EVAL_TIMEOUT_TEXT.format(expression))
    else:
        if result is None:
            await client.edit_message_text(
                m.chat.id,
                m.message_id,
                EVAL_SUCCESS_TEXT.format(expression)
            )
        else:
            await client.edit_message_text(
                m.chat.id,
                m.message_id,
                EVAL_RESULT_TEXT.format(expression, result)
            )
