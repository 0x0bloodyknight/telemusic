import logging
from loguru import logger
from pyrogram.raw.types import UpdateGroupCall, UpdateGroupCallParticipants

from ..telemusic import Telemusic

log = logging.getLogger(__name__)


@Telemusic.on_raw_update(group=-1)
@logger.catch
async def group_call(client, update, user, chats):
    if not isinstance(update, (UpdateGroupCall, UpdateGroupCallParticipants)):
        return
    logger.debug(f"received {update}")
