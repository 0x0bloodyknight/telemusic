import time
import asyncio
import logging
import json

from loguru import logger
import pyrogram
from pyrogram.types import Message
from pyrogram.filters import command

from ..helpers import process_request, connected_only
from ..models import Song, User
from ..telemusic import Telemusic

log = logging.getLogger(__name__)


@Telemusic.on_message(command("join"))
async def join(client, message: Message):
    if client.is_call_connected(message.chat.id):
        await message.reply_text("already connected")
        return

    ok = await client.connect_voice(message.chat.id)
    if ok:
        await message.reply_text("connected.")
    else:
        await message.reply_text("voice chat not started")


@Telemusic.on_message(command("play"))
@connected_only(connect=True)
async def play(client, message: Message):
    req = " ".join(message.command[1:])
    
    title, url, duration = await process_request(req)
    if not await client.is_queue_empty(message.chat.id):
        await message.reply_text(f"added {url} to queue")
    user, _ = await User.get_or_create(id=message.from_user.id)
    user.username = message.from_user.username or "user"
    await user.save()
    song = await Song.create(title=title, url=url, duration=duration,
                             requested_by=user)
    await client.play(message.chat.id, song)


@Telemusic.on_message(command("skip"))
@connected_only()
async def skip(client, message: Message):
    if await client.is_queue_empty(message.chat.id):
        await message.reply_text("nothing is playing now.")
    else:
        await client.skip(message.chat.id)


@Telemusic.on_message(command("stop"))
@connected_only()
async def stop(client, message: Message):
    await client.stop_call(message.chat.id)
    await message.reply_text("disconnected")


@Telemusic.on_message(command(["now_playing", "np"]))
@connected_only()
async def now_playing(client, message: Message):
    song, place = await client.get_current_song(message.chat.id)
    if not song:
        await message.reply_text("nothing is playing now.")
    else:
        await message.reply_text(f"now playing: {song.url} [{place}/{song.duration}]\n\n"
                                 f"requested by: {song.requestor.username}\n")
