import sys
import logging

from pathlib import Path
from argparse import ArgumentParser, BooleanOptionalAction

from .logger import ColoredFormatter
from .bot import Telemusic


if __name__ == "__main__":
    h = logging.StreamHandler()
    h.setFormatter(ColoredFormatter())
    logging.basicConfig(handlers=[h], level=logging.INFO)
    
    parser = ArgumentParser("telemusic bot")
    parser.add_argument("--create-tables", default=False, help="create tables", action=BooleanOptionalAction)

    args = parser.parse_args()

    Telemusic(workdir=Path(sys.argv[0]).parent.parent).run(create_tables=args.create_tables)
