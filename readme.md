# telemusic
bot for playing music in telegram group calls

## status
### working:
- queues
- searching videos on youtube
- skipping songs

### not working:
- multiple chats
- pausing
- changing volume

### tgcalls module status
a dummy for future use

## setup
### requirements
python3.9
pip packages:
- pyrogram
- youtube-dl
- tortoise-orm
- aiosqlite (or any other asyncio database implementation which supported by tortoise-orm)

### setting up environment
1. create python3 virtual environment
    ```sh
    python -m venv .venv
    ```
2. activate virtual environment
    ```sh
    source .venv/bin/activate
    ```
3. install requirements
    ```sh
    pip install -r requirements.txt
    ```
4. edit config
   ```sh
   $EDITOR telemusic.ini
   ```
   change `api_id` and `api_hash`. you can get them from my.telegram.org
   also if you need change `db_url`
5. run 
    ```sh
    python -m telemusic
    ```
   on first run it will ask phone number

